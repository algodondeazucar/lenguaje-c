/*
 *Fecha: 21-08-2020 
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Realice un algoritmo para leer las calificaciones de N alumnos y de­termine el número de aprobados y reprobados.*/

/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	int aprobados = 0;
	int reprobados = 0;
	int cantidad;
	int calificacion;
	int i;

	printf("Ingrese la cantidad de calificaciones a evaluar: ");
	scanf("%d", &cantidad);

	for (i=1;i<=cantidad;i++){
		printf("\nIngrese la calificacion del alumno: ");
		scanf("%d", &calificacion);

		if(calificacion >= 6){/*Inicio condicional if-else*/
			aprobados = aprobados + 1;
		}
		 else {
		 	reprobados = reprobados + 1;
		 }/*Fin condicional if-else*/
	}/*Fin for_1*/
	printf("Hubo %d %s %d %s", aprobados ," alumnos aprobados y ", reprobados ," reprobados");
}/*Fin metodo principal*/

